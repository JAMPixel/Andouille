//
// Created by baptiste on 11/02/17.
//

#ifndef ANDOUILLE_USFORCE_H
#define ANDOUILLE_USFORCE_H

#include <glk/gl/sprite/Sprite.h>
#include <glk/gl/text/Text.h>
#include <glk/gl/text/TextProgram.h>
#include <glk/gl/text/FontFace.h>

struct USForce{
    USForce(glk::gl::TextProgram& txtProg,const glk::gl::FontFace& font,glm::vec2 pos, glm::vec2 posMin, glm::vec2 posMax);

    void moveTo(glm::vec2 pos);

    void move();

    glk::gl::Sprite sprite;
    glk::gl::Text nameTag;

    glm::vec2 posMin;
    glm::vec2 posMax;
};

#endif //ANDOUILLE_USFORCE_H
