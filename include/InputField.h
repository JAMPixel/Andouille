#pragma once

#include <glm/vec2.hpp>

#include <glk/gl/text/Text.h>

#include <SDL2/SDL.h>


struct Global;

struct InputField {
	
	InputField(Global *g, glm::vec2 pos, float size);
	
	void display();

	void key_pressed(SDL_Event ev);
	
	glk::gl::Text textField;
	std::string text;
};