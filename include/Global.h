#pragma once

#include <memory>

#include <glk/gl/text/FontFace.h>
#include <glk/gl/text/TextProgram.h>
#include <glk/gl/Texture.h>
#include <glk/gl/sprite/SpriteBatch.h>

#include <cstdlib>
#include <ctime>

struct Nazi;
struct Projectile;

#include "Dico.h"
#include "InputField.h"
#include "Player.h"
#include "USForce.h"

#define PERIODE_MIN_SPAWN_IN_FRAME 20
#define PERIODE_MAX_SPAWN 5 //in second
#define SCORE_PER_KILL 30
#define KILL_TO_LVL_UP 10
#define SPEED_MULT .002f
#define TIME_BEFORE_BEGIN 3 //in seconds
#define NUKE_TIME 60 //in frame
#define ALLIE_FREQUENCE 3 //(multiplie par KILL_TO_LVL_UP)
#define TEMPS_RECULE_APRES_COUP 60 //in frame

struct Global {
	Global();
	~Global();

    void input(std::string s);

	void display();

    void update();

    void addNazi();
	
	void kill(Nazi const *nazi);

	glk::gl::TextProgram txtProg;
	glk::gl::TextureName fontTex;
	glk::gl::FontFace font;

    InputField in;
	glk::gl::TextureName naziTex;
	glk::gl::TextureName bgTex;

	std::vector<std::unique_ptr<Nazi>> nazis;
	glk::gl::SpriteBatch naziBatch;
	
	glk::gl::SpriteBatch bgBatch;
	
	glk::gl::TextureName sickleTex;
	glk::gl::TextureName hammerTex;
	glk::gl::TextureName sovietTex;
	
	glk::gl::SpriteBatch sovietBatch;

    glk::gl::TextureName bouclierTex;
    glk::gl::SpriteBatch bouclierBatch;
    glk::gl::Sprite bouclierSprite;

	std::vector<std::unique_ptr<Projectile>> projectiles;
	std::pair<glk::gl::SpriteBatch,glk::gl::SpriteBatch> projectileBatch;
    bool nextType;

    std::unique_ptr<USForce> allie;
    glk::gl::TextureName allieTex;
    glk::gl::SpriteBatch allieBatch;
    glk::gl::TextureName nukeTex;
    glk::gl::SpriteBatch nukeBatch;
    glk::gl::Sprite nukeSprite;
    unsigned nuke_count;
	
	unsigned diffLvl;
    unsigned nbkill;

    Dico dico;

    Player joueur;
    unsigned score;
    bool replay;
    glk::gl::Text looseMsg;
    glk::gl::Text scoreTag;
    glk::gl::Text lifeTag;

	static int const winWidth, winHeight;
	static float const pxPerMeter, meterPerPx;
	static glm::vec2 const viewSize;

    bool close;
};

extern std::unique_ptr<Global> g;
