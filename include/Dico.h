#pragma once

#include <fstream>
#include <map>
#include <stdexcept>
#include <ctime>
#include <cstdlib>

#define TAILLE_MOT_MAX 45

struct Dico{
    std::multimap<int,std::string> _words;

    Dico(std::string fileName);
    std::string get(int a,int b);
};