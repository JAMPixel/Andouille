//
// Created by baptiste on 11/02/17.
//

#ifndef ANDOUILLE_PLAYER_H
#define ANDOUILLE_PLAYER_H

#include <glk/gl/sprite/Sprite.h>

#define HP_MAX 1000

struct Player{
    Player(float x,float y);
	
    unsigned life;

	glk::gl::Sprite sprite;
};
#endif //ANDOUILLE_PLAYER_H
