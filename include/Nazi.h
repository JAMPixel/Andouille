#pragma once

#include <string>

#include <glk/gl/sprite/Sprite.h>
#include <glk/gl/text/Text.h>

#define NAZI_DPS 30
#define VITESSE_POUR_FRAPPE 0.008f

struct Nazi {
	Nazi(glm::vec2 pos, std::string name, float speed);

	void moveTo(glm::vec2 pos);

    bool move(glm::vec2 destination, float rayon);
	
	glk::gl::Sprite sprite;
    glk::gl::Text nameTag;

	std::string name;
	float speed;
    unsigned recule;

    bool isDestroied;
};
