#pragma once

#include <glk/gl/sprite/Sprite.h>

struct Nazi;

struct Projectile {
	Projectile(bool type, glm::vec2 pos, Nazi const *target);
	
	void update();
	
	bool type;
	Nazi const *target;
	glk::gl::Sprite sprite;
	glm::vec2 speed;
	bool isDestroied;
};
