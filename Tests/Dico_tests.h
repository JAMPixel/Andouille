//
// Created by baptiste on 11/02/17.
//

#ifndef ANDOUILLE_DICO_TESTS_H
#define ANDOUILLE_DICO_TESTS_H

#include <iostream>

#include "../include/Dico.h"

void dico_tests(){
    Dico d("data/francais.txt");
    std::string res;

    for(int i=1;i<30;++i){
        res=d.get(i,i+2);
        if(res.length()){
            std::cout<<res<<std::endl;
        }
    }
}

#endif //ANDOUILLE_DICO_TESTS_H
