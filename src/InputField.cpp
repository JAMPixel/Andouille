#include "InputField.h"

#include <glk/gl/text/TextProgram.h>

#include "Global.h"

InputField::InputField(Global *g, glm::vec2 pos, float size)
: textField{
	g->txtProg,
    g->font,
    pos,
    size,
	{0.0f, 1.0f}
} { }

void InputField::display() {
	textField.display();
}

namespace {
	char transcode(std::string mbc) {
		if(mbc[0] == '\xc3') // Accents UTF-8 -> Latin-1
			return (unsigned char)0x40 | (unsigned char)mbc[1];
		
		return mbc[0];
	}
}

void InputField::key_pressed(SDL_Event ev){
    char *c;
    if(ev.type==SDL_TEXTINPUT){
        c=ev.text.text;
        //TODO verifier c
		if (text.length()+1 <= TAILLE_MOT_MAX) {
			text+= transcode(c);
			textField.text(text);
		}
	}else if(ev.type == SDL_KEYDOWN){
        if(ev.key.keysym.sym == SDLK_BACKSPACE) {
	        if(!text.empty()) {
		        text.pop_back();
		        textField.text(text);
	        }
        }else if(ev.key.keysym.sym == SDLK_RETURN){
            g->input(text);
            text="";
            textField.text("");
        }
    }
}
