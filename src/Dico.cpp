//
// Created by baptiste on 11/02/17.
//

#include "Dico.h"

Dico::Dico(std::string fileName) : _words(std::multimap<int,std::string>()){
    std::string line;
    std::ifstream file(fileName,std::ios::in);

    if(file){
        while(getline(file,line)){
            _words.insert(std::pair<int,std::string>(line.length(),line));
        }
    }else{
        throw std::runtime_error("Erreur : l'ouverture du fichier dictionnaire "+fileName+" a echoue.");
    }
}

std::string Dico::get(int a,int b){
    int key=rand()%(b-a);
    int val = 0;
    std::multimap<int,std::string>::iterator it;
    int count = 0;

    //recherche d'une clef non vide dans l'interval
    while(_words.count(key+a)==0 && count<b-a){
        key=(key+1)%(b-a);
        ++count;
    }

    //recherche d'une clef non vide proche de l'interval
    if(_words.count(key+a)==0){
        key=-1;
        while(key+a && _words.count(key+a)==0){
            --key;
        }
        if(key+a==0){
            key=b-a;
            while(_words.count(key+a)==0 && key+a<=TAILLE_MOT_MAX){
                ++key;
            }
        }
    }

    if(_words.count(key+a)) {
        it= _words.find(key+a);
        val=rand()%(static_cast<int>(_words.count(key+a)));

        for (int i = 0; i < val; ++i,++it);

        return it->second;
    }else{
        return "";
    }
}