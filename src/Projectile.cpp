#include "Projectile.h"

#include "Nazi.h"
#include "Global.h"

namespace {
	constexpr float projectileSpeed = 0.35f, projectileReturnForce = 0.02f;
}

Projectile::Projectile(bool type, glm::vec2 pos, const Nazi *target)
: type{type}, target{target} , isDestroied(false){
	sprite.origin = glm::vec2{0.5f};
	sprite.position = pos;
}

void Projectile::update() {
	sprite.angle += 37.0f;
	
	if(target) {
		glm::vec2 dist = target->sprite.position - sprite.position;
		
		if(glm::dot(dist, dist) < glk::sqr(1.0f)) {
			g->kill(target);
			target = nullptr;
		} else {
			speed = projectileSpeed * glm::normalize(dist);
		}
	}
	
	if(!target) {
		glm::vec2 attractDir = glm::normalize(g->joueur.sprite.position - sprite.position);
		speed += projectileReturnForce * attractDir;
		
		if(glm::dot(speed, speed) > glk::sqr(projectileSpeed))
			speed = projectileSpeed * glm::normalize(speed);
	}
	
	sprite.position += speed;
}
