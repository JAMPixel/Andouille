#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <glk/path.h>
#include <glk/gluon.h>
#include <glk/GameLoop.h>
#include <glk/scopeExit.h>

#include <glk/gl/glChk.h>
#include <glk/gl/Texture.h>
#include <glk/gl/sprite/SpriteBatch.h>
#include <glk/gl/sprite/Sprite.h>
#include <glk/gl/sprite/SpriteProgram.h>
#include <glk/gl/Sampler.h>
#include <glk/gl/text/TextProgram.h>

#include "Global.h"
#include "InputField.h"
#include "Nazi.h"

namespace gluon = glk::gluon;
namespace gl = glk::gl;

int main(int, char **) {
	gluon::Sdl const sdl{SDL_INIT_VIDEO | SDL_INIT_EVENTS};
	
	if((IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG) & (IMG_INIT_JPG | IMG_INIT_PNG)) != (IMG_INIT_JPG | IMG_INIT_PNG))
		return -1;
	
	constexpr int screen = 1;
	
	auto const sdlWindow = gluon::createWindow(
		"Tonton Staline : Nazi Genocide",
		SDL_WINDOWPOS_CENTERED_DISPLAY(screen), SDL_WINDOWPOS_CENTERED_DISPLAY(screen),
		Global::winWidth, Global::winHeight,
		SDL_WINDOW_OPENGL
	);
	
	auto const glContext = gluon::createGlContext(sdlWindow.get(), 3, 3);
	
	gluon::initGlew();
	
	std::cout << "Initialized OpenGL " << gluon::glContextVersion() << std::endl;
		
	glChk glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glChk glEnable(GL_BLEND);
	glChk glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	auto nnfSampler = glk::gl::makeNnfSampler();
	bindSamplerEverywhere(nnfSampler);
	
	g = std::make_unique<Global>();
	glk_scopeExit { g.reset(); };
	
	glm::mat3 const worldToView{
		1.0f / Global::viewSize.x, 0.0f, 0.0f,
		0.0f, 1.0f / Global::viewSize.y, 0.0f,
		0.0f, 0.0f, 1.0f
	};
	
	glm::mat3 const viewToScreen{
		2.0f, 0.0f, 0.0f,
		0.0f, -2.0f, 0.0f,
		-1.0f, 1.0f, 1.0f
	};
	
	glk_with(bind(gl::defaultSpriteProgram())) {
		gl::defaultSpriteProgram().pvMatrix = viewToScreen * worldToView;
	}
	
	glk_with(bind(g->txtProg)) {
		g->txtProg.pvMatrix = viewToScreen * worldToView;
		g->txtProg.color = glm::vec3{1.0f};
	}
	
	auto tex = gl::load2dTex("data/tex.png"_f);
	
	gl::SpriteBatch sb{tex};
	
	gl::Sprite spr;
	spr.origin = glm::vec2{0.5f};

	float a = 0.0f;
	
	glk::GameLoop loop{60};
	
	SDL_Event ev;
    SDL_StartTextInput();
	do {
		while (SDL_PollEvent(&ev)) {
			if (ev.type == SDL_QUIT
				|| (ev.type == SDL_KEYDOWN && ev.key.keysym.scancode == SDL_SCANCODE_ESCAPE))
				loop.stop();
			else{
                g->in.key_pressed(ev);
				if(g->close){
					loop.stop();
				}
			}
		}
		
		a = glk::loop(a + 10.0f, 0.0f, 360.0f);
		
		spr.size = glm::vec2{1.0f} + std::cos(glk::deg2rad(a)) * 0.1f * glm::vec2{1.0f, -1.0f};
		
		sb.push_back(spr.makeAttribs());
		
		glChk glClear(GL_COLOR_BUFFER_BIT);
		
		sb.displayAndClear();
        g->update();
		g->display();

		SDL_GL_SwapWindow(sdlWindow.get());
	} while (loop.endFrame());
}
