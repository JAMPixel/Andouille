//
// Created by baptiste on 11/02/17.
//

#include "Player.h"

Player::Player(float x, float y)
: life(HP_MAX)
{
	sprite.origin = glm::vec2{0.5f};
	sprite.position = glm::vec2{x, y};
}
