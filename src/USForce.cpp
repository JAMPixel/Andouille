#include "USForce.h"

#include "Global.h"

USForce::USForce(glk::gl::TextProgram& txtProg,const glk::gl::FontFace& font,glm::vec2 pos,glm::vec2 posMin,glm::vec2 posMax)
: nameTag{txtProg, font, glm::vec2{0.0f, 0.0f}, 0.5f, glm::vec2{0.5f}}
, posMin(posMin)
, posMax(posMax){
    sprite.origin = glm::vec2{0.5f};
    nameTag.text("Rambo");
    moveTo(pos);
}

void USForce::moveTo(glm::vec2 pos) {
    float textpos=-0.75f;
	sprite.position = pos;

    if(sprite.position.y<=1){
        textpos=.5f;
    }
	nameTag.transform(pos + glm::vec2{0.0f, textpos}, 0.5f);
}

void USForce::move() {
    static float dpx = -0.02f, dpy=0.f;
    int signeX, signeY;
    glm::vec2 dest{sprite.position.x,sprite.position.y};

    if(!(rand()%60)){
        signeX = rand()%2;
        signeY = rand()%2;
        if(!signeX)
            signeX = -1;
        if(!signeY)
            signeY = -1;

        dpx=rand()*1.f/RAND_MAX * 0.02f;
        dpy=0.02f - dpx;

        dpx*=signeX;
        dpy*=signeY;
    }

    dest.x+=dpx;
    dest.y+=dpy;

    if(dest.y<0.5f+posMin.y)
        dest.y=0.5f+posMin.y;
    else if(dest.y>posMax.y-0.5f)
        dest.y=posMax.y-0.5f;
    if(dest.x<0.5f+posMin.x)
        dest.x=0.5f+posMin.x;
    else if(dest.x>posMax.x-0.5f)
        dest.x=posMax.x-0.5f;

    moveTo(dest);
}
