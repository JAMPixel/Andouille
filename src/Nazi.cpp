#include <cmath>

#include "Nazi.h"

#include "Global.h"

Nazi::Nazi(glm::vec2 pos, std::string name, float speed)
: nameTag{g->txtProg, g->font, glm::vec2{0.0f, 0.0f}, 0.5f, glm::vec2{0.5f}} , name{name} , speed(speed) , recule(0) , isDestroied(false) {
	sprite.origin = glm::vec2{0.5f};
	nameTag.text(name);
	moveTo(pos);
}

void Nazi::moveTo(glm::vec2 pos) {
    float textpos=-0.75f;
	sprite.position = pos;

    if(sprite.position.y<=1){
        textpos=.5f;
    }
	nameTag.transform(pos + glm::vec2{0.0f, textpos}, 0.5f);
}

bool Nazi::move(glm::vec2 destination, float rayon) {
    double angle=atan2(static_cast<double>(sprite.position.y-destination.y), static_cast<double>(sprite.position.x-destination.x));
    float angleX= static_cast<float>(cos(angle));
    float angleY = static_cast<float>(sin(angle));
    bool resultat;
    int signe = 1;

    if(recule){
        speed=VITESSE_POUR_FRAPPE;
        --recule;
        signe=-1;
    }

    glm::vec2 vect{sprite.position.x,sprite.position.y};

    vect.x-=signe*angleX*(vect.x-destination.x+rayon>=speed?speed:vect.x-destination.x+rayon);
    if(vect.y>destination.y)
        vect.y-=signe*angleY*(vect.y-destination.y>=speed?speed:vect.y-destination.y);
    else
        vect.y-=signe*angleY*(-vect.y+destination.y>=speed?speed:-vect.y+destination.y);

    if(vect.x<destination.x+angleX*rayon){
        vect.x=destination.x+angleX*rayon;
    }
    if(fabsf(vect.y-destination.y)<fabsf(angleY*rayon)){
        vect.y=destination.y+angleY*rayon;
    }

    resultat=vect.x==sprite.position.x && vect.y==sprite.position.y;

    moveTo(vect);

    return resultat;
}
