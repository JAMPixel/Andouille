#include "Global.h"

#include <glk/path.h>
#include <glk/gl/Shader.h>

#include <string>

#include "Nazi.h"
#include "Projectile.h"

std::unique_ptr<Global> g;

int const Global::winWidth = 1024, Global::winHeight = 768;
float const Global::pxPerMeter = 64.0f, Global::meterPerPx = 1.0f / Global::pxPerMeter;
glm::vec2 const Global::viewSize{winWidth * meterPerPx, winHeight * meterPerPx};

glk::gl::FontFace monoFont(glk::gl::TextureName const &tex) {
	glk::gl::FontFace ftf{&tex};
	
	auto &fontMap = ftf.map;
	auto &texEm = ftf.emSquare;
	
	texEm = glm::vec2{1.0f / 16.0f};
	
	for(int y = 0; y < 16; ++y)
		for(int x = 0; x < 16; ++x)
			fontMap[16 * y + x] = {
				{x / 16.0f, y / 16.0f}, // tl
				{(x + 1) / 16.0f, (y + 1) / 16.0f}, // br
				glm::vec2{0.0f, -15 / 256.0f} / texEm, // orig
				8.0f / 256.0f / texEm.x // adv
			};
	
	fontMap['"'].adv = 7.0f / 256.0f / texEm.x;
	fontMap['$'].adv = 7.0f / 256.0f / texEm.x;
	fontMap['%'].adv = 7.0f / 256.0f / texEm.x;
	fontMap['+'].adv = 7.0f / 256.0f / texEm.x;
	fontMap['-'].adv = 7.0f / 256.0f / texEm.x;
	fontMap['/'].adv = 7.0f / 256.0f / texEm.x;
	fontMap['1'].adv = 7.0f / 256.0f / texEm.x;
	fontMap['I'].adv = 7.0f / 256.0f / texEm.x;
	fontMap['T'].adv = 7.0f / 256.0f / texEm.x;
	fontMap['Y'].adv = 7.0f / 256.0f / texEm.x;
	fontMap['\\'].adv = 7.0f / 256.0f / texEm.x;
	fontMap['k'].adv = 7.0f / 256.0f / texEm.x;
	fontMap['1'].adv = 7.0f / 256.0f / texEm.x;
	
	fontMap['1'].adv = 6.0f / 256.0f / texEm.x;
	fontMap['<'].adv = 6.0f / 256.0f / texEm.x;
	fontMap['>'].adv = 6.0f / 256.0f / texEm.x;
	fontMap['f'].adv = 6.0f / 256.0f / texEm.x;
	fontMap['{'].adv = 6.0f / 256.0f / texEm.x;
	fontMap['}'].adv = 6.0f / 256.0f / texEm.x;
	
	fontMap['!'].adv = 5.0f / 256.0f / texEm.x;
	fontMap['('].adv = 5.0f / 256.0f / texEm.x;
	fontMap[')'].adv = 5.0f / 256.0f / texEm.x;
	fontMap['='].adv = 5.0f / 256.0f / texEm.x;
	fontMap['['].adv = 5.0f / 256.0f / texEm.x;
	fontMap[']'].adv = 5.0f / 256.0f / texEm.x;
	fontMap['i'].adv = 5.0f / 256.0f / texEm.x;
	fontMap['j'].adv = 5.0f / 256.0f / texEm.x;
	fontMap['l'].adv = 5.0f / 256.0f / texEm.x;
	
	fontMap[','].adv = 4.0f / 256.0f / texEm.x;
	fontMap[';'].adv = 4.0f / 256.0f / texEm.x;
	fontMap['`'].adv = 4.0f / 256.0f / texEm.x;
	
	fontMap['\''].adv = 3.0f / 256.0f / texEm.x;
	fontMap['.'].adv = 3.0f / 256.0f / texEm.x;
	fontMap[':'].adv = 3.0f / 256.0f / texEm.x;
	fontMap['|'].adv = 3.0f / 256.0f / texEm.x;
	
	return ftf;
}

namespace {
	std::string const txtVert = R"~(
		#version 330
	
		uniform mat3 pvMatrix;
		uniform mat3x2 transform;
		uniform vec2 texEm;
	
		in vec2 vertexCoord;
		in vec2 tlUv;
		in vec2 brUv;
		in vec2 offset;
	
		out VSOut {
			vec2 uv;
			flat int instanceId;
		} _out;
	
		void main() {
			vec2 size = (brUv - tlUv) / texEm;
			gl_Position = vec4(pvMatrix * mat3(transform) * vec3(vertexCoord * size + offset, 1.0f), 1.0);
			_out.uv = mix(tlUv, brUv, vertexCoord);
			_out.instanceId = gl_InstanceID;
		}
	)~";
	
	std::string const txtFrag = R"~(
		#version 330
	
		uniform sampler2D fontTex;
		uniform vec3 color;
	
		in VSOut {
			vec2 uv;
			flat int instanceId;
		} _in;
	
		out vec4 col;
	
		void main() {
			col = vec4(color, 1.0) * texture(fontTex, _in.uv);
		}
	)~";
}

Global::Global()
: txtProg{glk::gl::VertShader{txtVert}, glk::gl::FragShader{txtFrag}}
, fontTex{glk::gl::load2dTex("data/font.png"_f)}
, font{monoFont(fontTex)}
, in{this, {0.1f,viewSize.y - 0.1f}, 1.0f}
, naziTex{glk::gl::load2dTex("data/nazi.png"_f)}
, bgTex{glk::gl::load2dTex("data/neige.png"_f)}
, naziBatch{naziTex}
, bgBatch{bgTex}
, sickleTex{glk::gl::load2dTex("data/sickle.png"_f)}
, hammerTex{glk::gl::load2dTex("data/hammer.png"_f)}
, sovietTex{glk::gl::load2dTex("data/soviet.png"_f)}
, sovietBatch{sovietTex}
, bouclierTex{glk::gl::load2dTex("data/bouclier.png"_f)}
, bouclierBatch{bouclierTex}
, projectileBatch{sickleTex,hammerTex}
, nextType(false)
, allie(new USForce(txtProg,font,{-1.f,-1.f},{3.f,0.f},{viewSize.x-1,viewSize.y-1}))
, allieTex{glk::gl::load2dTex("data/usa.png"_f)}
, allieBatch(allieTex)
, nukeTex{glk::gl::load2dTex("data/nuke.png"_f)}
, nukeBatch(nukeTex)
, nuke_count(0)
, diffLvl(1)
, nbkill(0)
, dico("data/allemand.txt")
, joueur(1.f,viewSize.y/2)
, score(0)
, replay(false)
, looseMsg{txtProg, font, glm::vec2{0.0f, 0.0f}, 0.5f, glm::vec2{0.5f}}
, scoreTag{txtProg, font, glm::vec2{0.0f, 0.0f}, 0.5f, glm::vec2{0.5f}}
, lifeTag{txtProg, font, glm::vec2{0.0f, 0.0f}, 0.5f, glm::vec2{0.5f}}
, close(false){

	glk::gl::Sprite bgSpr;
	bgSpr.size = viewSize;
	bgBatch.push_back(bgSpr.makeAttribs());
    srand(time(0));

    bouclierSprite.position.x=joueur.sprite.position.x-1.f;
    bouclierSprite.position.y=joueur.sprite.position.y-2.f;
    bouclierSprite.size.x=joueur.sprite.size.x+3.f;
    bouclierSprite.size.y=joueur.sprite.size.y+3.f;

    looseMsg.text("Oh no ! You were killed by the Nazi !");
    looseMsg.transform({viewSize.x/2,1.f},.75f);

    scoreTag.text(std::to_string(0));
    scoreTag.transform({viewSize.x/2,viewSize.y-1.f},.75f);

    lifeTag.text(std::to_string(HP_MAX));
    lifeTag.transform({3*viewSize.x/4,viewSize.y-1.f},.75f);
}

Global::~Global() = default;

void Global::display() {
    unsigned id = 0;

	bgBatch.display();
	
	sovietBatch.push_back(joueur.sprite.makeAttribs());
	sovietBatch.displayAndClear();

    bouclierBatch.push_back(bouclierSprite.makeAttribs());
    bouclierBatch.displayAndClear();

	for(auto const &nazi : nazis)
		naziBatch.push_back(nazi->sprite.makeAttribs());
	naziBatch.displayAndClear();
	
	for(auto const &proj : projectiles) {
        if(proj->type)
            projectileBatch.second.push_back(proj->sprite.makeAttribs());
        else
            projectileBatch.first.push_back(proj->sprite.makeAttribs());
    }
	projectileBatch.first.displayAndClear();
	projectileBatch.second.displayAndClear();
	
	in.display();
	
	for(auto const &nazi : nazis)
		nazi->nameTag.display();

    if(allie->sprite.position.x!=-1.f) {
        if(nuke_count!=0){
            nukeBatch.push_back(nukeSprite.makeAttribs());
            nukeBatch.displayAndClear();
        }else {
            allieBatch.push_back(allie->sprite.makeAttribs());
            allieBatch.displayAndClear();

            allie->nameTag.display();
        }
    }

    if(joueur.life==0)
        looseMsg.display();
    scoreTag.display();
    lifeTag.display();

    while(id<nazis.size()){
        if(nazis[id].get()->isDestroied) {
            swap(nazis[id], nazis.back());
            nazis.pop_back();
        }else{
            ++id;
        }
    }
    while(id<projectiles.size()){
        if(projectiles[id].get()->isDestroied) {
            swap(projectiles[id], projectiles.back());
            projectiles.pop_back();
        }else{
            ++id;
        }
    }
}

void Global::input(std::string s) {
    if(s == "exit"){
        close=true;
    }else if(s == "replay"){
        replay=true;
    }else if (s == "Rambo" && allie->sprite.position.x != -1.f) {
        for (const auto& it : nazis) {
            it->isDestroied=true;
            it->sprite.position.x=-4.f;

            score += SCORE_PER_KILL / 2;
            nbkill++;
        }
        scoreTag.text(std::to_string(score));
        while(nbkill>=KILL_TO_LVL_UP) {
            diffLvl++;
            nbkill-=KILL_TO_LVL_UP;
        }
        nuke_count=NUKE_TIME;
        nukeSprite=allie->sprite;
    }else if(projectiles.size()<2) {
        auto matchingNazi = std::find_if(begin(nazis), end(nazis), [&](auto const &nazi) {
            return nazi->name == s;
        });

        if (matchingNazi == end(nazis))
            return;

        projectiles.push_back(std::make_unique<Projectile>(nextType, joueur.sprite.position, matchingNazi->get()));
        nextType=!nextType;
    }
}

void Global::addNazi() {
    float speed;

    if(allie->sprite.position.x!=-1.f || rand()%(KILL_TO_LVL_UP*ALLIE_FREQUENCE)) {
        speed = (3 * diffLvl + rand() % (2 * diffLvl + 1) - diffLvl) * SPEED_MULT;
        nazis.push_back(
                std::make_unique<Nazi>(glm::vec2{viewSize.x - 1, 1.0f * rand() / RAND_MAX * (viewSize.y - 1) + 0.5f},
                                       dico.get(2 * diffLvl, 3 * diffLvl), speed));
    } else {
        allie->sprite.position={viewSize.x - 1, 1.0f * rand() / RAND_MAX * (viewSize.y - 1) + 0.5f};
    }
}

void Global::kill(Nazi const *nazi) {
	auto ite = std::find_if(begin(nazis), end(nazis), [&](auto const &np) {
		return np.get() == nazi;
	});
	
	// assert(ite != end(nazis));
	if(ite!=end(nazis)) {
        ite->get()->isDestroied=true;
        ite->get()->sprite.position.x=-4.f;

        score += SCORE_PER_KILL;
        scoreTag.text(std::to_string(score));

        nbkill++;

        // augmentation de la difficulte en fonction du nombre de nazi detruit
        if (nbkill >= KILL_TO_LVL_UP) {
            diffLvl++;
            nbkill -= KILL_TO_LVL_UP;
        }
    }
}

void Global::update() {
    static unsigned spawn_nazi = 60*TIME_BEFORE_BEGIN;
    int tps;
    glm::vec2 d;

    if(!replay && joueur.life && !nuke_count) {
        if (allie->sprite.position.x > 0)
            allie->move();

        // ajout d'un nazi
        if (!spawn_nazi) {
            addNazi();
            tps = PERIODE_MAX_SPAWN * 60 - 20 * (diffLvl - 1); // temps de spawn selon niveau de difficulte
            spawn_nazi = (tps < PERIODE_MIN_SPAWN_IN_FRAME ? PERIODE_MIN_SPAWN_IN_FRAME : static_cast<unsigned>(tps));
        } else {
            --spawn_nazi;
        }

        // deplacement des projectils
        for (auto const &proj : projectiles)
            proj->update();

        // destruction des objets armes
        for(const auto& proj : projectiles) {
            if (!proj->target) {
                if (proj->sprite.position.x<=joueur.sprite.position.x+1.f) {
                    proj->sprite.position.x = -4.f;
                    proj->isDestroied = true;
                }
            }
        }

        // dommages occasionnes par les nazis + deplacement des nazis
        for (auto it = nazis.begin(); it != nazis.end(); ++it)
            if ((*it)->move(joueur.sprite.position, 2.0f)) {
                joueur.life = joueur.life - NAZI_DPS;
                (*it)->recule=TEMPS_RECULE_APRES_COUP;
                if (joueur.life > HP_MAX)
                    joueur.life = 0;
                lifeTag.text(std::to_string(joueur.life));
            }
    }else if(!replay && nuke_count!=0){
        nukeSprite.size.x=(NUKE_TIME-nuke_count*1.f)/NUKE_TIME*(2*viewSize.y-1)+1.f;
        nukeSprite.size.y=nukeSprite.size.x;
        --nuke_count;
        if(nuke_count==0)
            allie->sprite.position = {-1.f, -1.f};
    }else if(replay){
        nbkill=0;
        score=0;
        scoreTag.text(std::to_string(0));
        diffLvl=1;
        nextType=false;
        while(nazis.size()) {
            nazis.pop_back();
        }
        allie->sprite.position.x=-1.f;
        joueur.life=HP_MAX;
        lifeTag.text(std::to_string(joueur.life));
        spawn_nazi=60*TIME_BEFORE_BEGIN;
        replay=false;
    }
}